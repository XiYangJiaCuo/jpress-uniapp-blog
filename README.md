# jpress-uniapp-blog
#### 感谢star，有问题可咨询作者，微信如下：
- 海天一线
![输入图片说明](https://images.gitee.com/uploads/images/2020/0208/104206_4b1bba72_451410.png "屏幕截图.png")

#### 介绍
- 后台基于jpress的uniapp博客小程序、H5博客；
- 支持微信等各类小程序、H5、APP；
- 已实现移动端动态读取后台博客（即jpress中的文章），后台录入、编辑后前端自动加载变化；
- 此版为基础版本，高级版本正在开发，包括博客分类显示、滑动切换顶部分类选项卡、上拉加载更多（分页）、个人中心微信小程序授权、登录、获取jpress用户信息等功能，效果如下：（可联系作者咨询，文章末尾微信二维码添加好友）
![输入图片说明](https://images.gitee.com/uploads/images/2020/0208/103321_611b869f_451410.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0208/103354_b337139a_451410.png "屏幕截图.png")

#### 基础版页面介绍：
1. 实现底部tab切换
2. 博客列表
3. 博客详情
4. 个人中心页面

#### 基础版效果图：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0208/103448_b4c88094_451410.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0208/103517_cd789329_451410.png "屏幕截图.png")

#### 软件架构
- 后台：jpress （开源地址：https://gitee.com/fuhai/jpress）
- 前台：uniapp（支持微信等各类小程序、H5、APP，官网：https://uniapp.dcloud.io/）
- 前台关键SDK：JPress 微信小程序SDK（开源地址：https://gitee.com/fuhai/jpress-miniprogram-sdk）


#### 安装教程

1.  下载代码，导入到HbuilderX
2.  修改App.vue中的jpress.init信息为自己的jpress后台地址，如下：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0208/101900_3d525c20_451410.png "屏幕截图.png")
- 注意，其中的app_id和app_secret，是在jpress后台设置的，如下：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0208/101930_29948054_451410.png "屏幕截图.png")
3.  使用HbuilderX工具运行为H5或者运行到微信开发者工具
4.  注意，如果是发布成微信小程序，小程序开发者ID需要改成自己的

#### 使用说明

1.  运行即可，全端适配，也可以快速发布到个人微信小程序上，通过内网穿透到本地开发环境，不懂可联系作者
2.  欢迎star，有问题可咨询作者，微信如下（海天一线）：
- 微信二维码：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0208/104206_4b1bba72_451410.png "屏幕截图.png")


